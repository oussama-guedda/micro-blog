<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('accueil/accueil.html.twig');
    }

    /**
     * @Route("/home", name="home_direction")
     */
    public function redirectAction()
    {
        $posts = $this->getDoctrine()->getRepository(Post::class)->findBy([], ['createdAt' => 'DESC']);
        $comments = $this->getDoctrine()->getRepository(Comment::class)->findAll();

        return $this->render('@App/index.html.twig', [
            'posts' => $posts,
            'comments' => $comments
        ]);
    }

}