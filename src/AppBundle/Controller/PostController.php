<?php


namespace AppBundle\Controller;


use AppBundle\Entity\Post;
use AppBundle\Form\PostType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends Controller
{
    /**
     * @Route("/post/create", name="create_post")
     */
    public function newAction(Request $request) {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);

        if($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $user = $this->getUser();
            $post->setUser($user);
            $post->setCreatedAt(new \DateTime('now'));
            $post->setUpdatedAt(new \DateTime('now'));

            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush($post);

            return $this->redirectToRoute('home_direction');
        }


        return $this->render('@App/post/create.html.twig', [
            'form' => $form->createView()
        ]);
    }
}