<?php


namespace AppBundle\Controller;


use AppBundle\Entity\Comment;
use AppBundle\Entity\Post;
use AppBundle\Form\CommentType;
use AppBundle\Form\PostType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CommentController extends Controller
{
    /**
     * @Route("/comment/create/{id}", name="create_comment")
     */
    public function newAction(Post $post, Request $request) {
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);

        if($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $user = $this->getUser();
            $comment->setUser($user);
            $comment->setPost($post);
            $comment->setCreatedAt(new \DateTime('now'));
            $comment->setUpdatedAt(new \DateTime('now'));

            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush($comment);

            return $this->redirectToRoute('home_direction');
        }


        return $this->render('@App/comment/add.html.twig', [
            'form' => $form->createView()
        ]);
    }
}